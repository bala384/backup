echo "==================================" >> backup.txt
echo "The Offsite backup is complete using the backup.txt on OwnCloud" > backup.txt
echo "" >> backup.txt
echo "The first level directory listing of the backup is shown below" >> backup.txt
echo "PLEASE VERIFY IF EVERYTHING IS GETTING BACKED UP" >> backup.txt
echo "==================================" >> backup.txt
echo "" >> backup.txt
find /mnt/energyly/backup/ -maxdepth 2 -mindepth 2 -type d -exec ls -d "{}" \; >> backup.txt
sed -i 's/\/mnt\/energyly\/backup//g' backup.txt
sed -i 's/\// /g' backup.txt

echo "" >> backup.txt
echo "ANY SSH CONNECTION ERRORS ARE SHOWN BELOW = Check public keys have been added machines" >> backup.txt
echo "==================================" >> backup.txt
cat ssherrors.txt >> backup.txt

tr -d '\015' < backup.txt  | mail -s "Current offsite backup directory list" -a "From: devops@energyly.com" devops@energyly.com
